## sqlite-http-basic-auth-nginx-module

./configure --add-module=ModulesPath/sqlite-http-basic-auth-nginx-module

create table users(
   id INT PRIMARY KEY NOT NULL,
   uname  CHAR(50) NOT NULL,
   upasswd  CHAR(50) NOT NULL
);

INSERT INTO users (id,uname,upasswd) VALUES (1,'wupz','190007936');

location / {
	root WwwRootPath;
	index index.html index.htm;
	auth_sqlite_basic "Restricted Sqlite";
	auth_sqlite_basic_database_file /DatabaseName.db;
	auth_sqlite_basic_database_table  users;
	auth_sqlite_basic_table_user_column  uname;
	auth_sqlite_basic_table_passwd_column  upasswd;
}
