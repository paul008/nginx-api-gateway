rm -rf /etc/yum.repos.d/nginx.repo
touch /etc/yum.repos.d/nginx.repo
cat >> /etc/yum.repos.d/nginx.repo << EOF
[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/centos/\$releasever/\$basearch/
gpgcheck=0
enabled=1
EOF
yum clean all

#安装NGINX
yum install nginx -y

#创建Nginx日志目录
mkdir -p /data/logs/nginx
mkdir -p /data/backup/nginx-modules
chmod -R 755 /data/logs/nginx
chown -R nginx:nginx /data/logs/nginx

#备份,编辑Nginx配置文件
cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
sed -i 's#/var/log/nginx/error.log#/data/logs/nginx/error.log#' /etc/nginx/nginx.conf
sed -i 's#/var/log/nginx/access.log#/data/logs/nginx/access.log#' /etc/nginx/nginx.conf

#设置nginx开机启动
systemctl start nginx
systemctl enable nginx