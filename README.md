# Nginx-Api-Gateway

#### 介绍
{** Nginx-Api-Gateway 是结合Nginx + rds-json-nginx + drizzle-nginx-module 实现的高效 接口网关} 

#### 软件架构
Nginx 通过 drizzle-nginx-module 直接访问MySQL，实现 数据管理；

#### 安装步骤

1. /usr/bin/sh env.sh
2. /usr/bin/sh nginx.sh
3. /usr/bin/sh api.sh

#### 配置相关
/etc/nginx/conf.d/api.conf

```
upstream domain{
    drizzle_server localhost:3306 dbname=account password= user= protocol=mysql charset=utf8;
}

server{
    listen        443 ssl;
    server_name   api.domain.com;
    root  /data/www/api/public;

    ssl_certificate     /data/www/api/cert/1_api.domain.com_bundle.crt;
    ssl_certificate_key /data/www/api/cert/2_api.domain.com.key;
    ssl_session_timeout  5m;

    access_log  /data/www/api/logs/access.log  main;
    error_log  /data/www/api/logs/error.log  error;

    location /mysql/version {
        #执行sql语句
        drizzle_query "select version()";
        drizzle_pass domain;

        drizzle_connect_timeout    500ms; # default 60s
        drizzle_send_query_timeout 2s;    # default 60s
        drizzle_recv_cols_timeout  1s;    # default 60s
        drizzle_recv_rows_timeout  1s;    # default 60s

        #返回查询的结果，json格式数据
        rds_json_root data;
        rds_json on;
    }

    location /mysql/process {
        #执行sql语句
        drizzle_query "show processlist";
        drizzle_pass domain;

        drizzle_connect_timeout    500ms; # default 60s
        drizzle_send_query_timeout 2s;    # default 60s
        drizzle_recv_cols_timeout  1s;    # default 60s
        drizzle_recv_rows_timeout  1s;    # default 60s

        #返回查询的结果，json格式数据
        rds_json on;
    }

    include /data/www/api/nginx/*.conf;
}

server{
    listen        80;
    server_name   api.domain.com;
    return 301    https://$host$request_uri;
}

```