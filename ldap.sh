yum install -y openldap openldap-clients openldap-servers migrationtools  
cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG  
chown ldap. /var/lib/ldap/DB_CONFIG  
systemctl start slapd   
systemctl enable slapd

#{SSHA}nMj8DtqwNw22BCkn2JqGMjxgxn9DvEWn
touch chrootpw.ldif
echo "dn: olcDatabase={0}config,cn=config" >> chrootpw.ldif
echo "changetype: modify" >> chrootpw.ldif
echo "add: olcRootPW" >> chrootpw.ldif
echo "olcRootPW: {SSHA}nMj8DtqwNw22BCkn2JqGMjxgxn9DvEWn" >> chrootpw.ldif

ldapadd -Y EXTERNAL -H ldapi:/// -f chrootpw.ldif

cd /etc/openldap/schema
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f cosine.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f nis.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f collective.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f corba.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f core.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f duaconf.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f dyngroup.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f inetorgperson.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f java.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f misc.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f openldap.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f pmi.ldif  
ldapadd -Y EXTERNAL -H ldapi:/// -D "cn=config" -f ppolicy.ldif 

slappasswd
#{SSHA}EtVHe2QO/Y0XfYP8nauE9HfnizIK6ovM

cat >> /root/chdomain.ldif << EOF
dn: olcDatabase={1}monitor,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" read by dn.base="cn=Manager,dc=wuweiit,dc=com" read by * none

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcSuffix
olcSuffix: dc=wuweiit,dc=com

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcRootDN
olcRootDN: cn=Manager,dc=wuweiit,dc=com

dn: olcDatabase={2}hdb,cn=config
changetype: modify
add: olcRootPW
olcRootPW: {SSHA}EtVHe2QO/Y0XfYP8nauE9HfnizIK6ovM

dn: olcDatabase={2}hdb,cn=config
changetype: modify
add: olcAccess
olcAccess: {0}to attrs=userPassword,shadowLastChange by dn="cn=Manager,dc=wuweiit,dc=com" write by anonymous auth by self write by * none
olcAccess: {1}to dn.base="" by * read
olcAccess: {2}to * by dn="cn=Manager,dc=wuweiit,dc=com" write by * read
EOF

ldapmodify -Y EXTERNAL -H ldapi:/// -f chdomain.ldif

cat >> /root/basedomain.ldif << EOF
dn: dc=wuweiit,dc=com
objectClass: top
objectClass: dcObject
objectclass: organization
o: Server World
dc: wuweiit

dn: cn=Manager,dc=wuweiit,dc=com
objectClass: organizationalRole
cn: Manager
description: Directory Manager

dn: ou=People,dc=wuweiit,dc=com
objectClass: organizationalUnit
ou: People

dn: ou=Group,dc=wuweiit,dc=com
objectClass: organizationalUnit
ou: Group
EOF

ldapadd -x -D cn=Manager,dc=wuweiit,dc=com -W -f basedomain.ldif


cat >> /root/ldapuser.ldif << EOF
dn: uid=cent,ou=People,dc=wuweiit,dc=com
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
cn: Cent
sn: Linux
userPassword: {SSHA}5XfTD7MIk+vZ/q9Xw9LPkB45yRTsBBcd
loginShell: /bin/bash
uidNumber: 1000
gidNumber: 1000
homeDirectory: /home/cent

dn: cn=cent,ou=Group,dc=wuweiit,dc=com
objectClass: posixGroup
cn: Cent
gidNumber: 1000
memberUid: cent
EOF

ldapadd -x -D cn=Manager,dc=wuweiit,dc=com -W -f ldapuser.ldif

cat >> ldapuser.sh << EOF
#!/bin/bash
# extract local users and groups who have 1000-9999 digit UID
# replace "SUFFIX=***" to your own domain name
# this is an example
  
SUFFIX='dc=wuweiit,dc=com'
LDIF='ldapusers.ldif'

echo -n > $LDIF
GROUP_IDS=()
grep "x:[1-9][0-9][0-9][0-9]:" /etc/passwd | (while read TARGET_USER
do
    USER_ID="$(echo "$TARGET_USER" | cut -d':' -f1)"
    USER_NAME="$(echo "$TARGET_USER" | cut -d':' -f5 | cut -d' ' -f1,2)"
    [ ! "$USER_NAME" ] && USER_NAME="$USER_ID"

    LDAP_SN="$(echo "$USER_NAME" | cut -d' ' -f2)"
    [ ! "$LDAP_SN" ] && LDAP_SN="$USER_NAME"

    LASTCHANGE_FLAG="$(grep "${USER_ID}:" /etc/shadow | cut -d':' -f3)"
    [ ! "$LASTCHANGE_FLAG" ] && LASTCHANGE_FLAG="0"

    SHADOW_FLAG="$(grep "${USER_ID}:" /etc/shadow | cut -d':' -f9)"
    [ ! "$SHADOW_FLAG" ] && SHADOW_FLAG="0"

    GROUP_ID="$(echo "$TARGET_USER" | cut -d':' -f4)"
    [ ! "$(echo "${GROUP_IDS[@]}" | grep "$GROUP_ID")" ] && GROUP_IDS=("${GROUP_IDS[@]}" "$GROUP_ID")

    echo "dn: uid=$USER_ID,ou=People,$SUFFIX" >> $LDIF
    echo "objectClass: inetOrgPerson" >> $LDIF
    echo "objectClass: posixAccount" >> $LDIF
    echo "objectClass: shadowAccount" >> $LDIF
    echo "sn: $LDAP_SN" >> $LDIF
    echo "givenName: $(echo "$USER_NAME" | awk '{print $1}')" >> $LDIF
    echo "cn: $USER_NAME" >> $LDIF
    echo "displayName: $USER_NAME" >> $LDIF
    echo "uidNumber: $(echo "$TARGET_USER" | cut -d':' -f3)" >> $LDIF
    echo "gidNumber: $(echo "$TARGET_USER" | cut -d':' -f4)" >> $LDIF
    echo "userPassword: {crypt}$(grep "${USER_ID}:" /etc/shadow | cut -d':' -f2)" >> $LDIF
    echo "gecos: $USER_NAME" >> $LDIF
    echo "loginShell: $(echo "$TARGET_USER" | cut -d':' -f7)" >> $LDIF
    echo "homeDirectory: $(echo "$TARGET_USER" | cut -d':' -f6)" >> $LDIF
    echo "shadowExpire: $(passwd -S "$USER_ID" | awk '{print $7}')" >> $LDIF
    echo "shadowFlag: $SHADOW_FLAG" >> $LDIF
    echo "shadowWarning: $(passwd -S "$USER_ID" | awk '{print $6}')" >> $LDIF
    echo "shadowMin: $(passwd -S "$USER_ID" | awk '{print $4}')" >> $LDIF
    echo "shadowMax: $(passwd -S "$USER_ID" | awk '{print $5}')" >> $LDIF
    echo "shadowLastChange: $LASTCHANGE_FLAG" >> $LDIF
    echo >> $LDIF
done

for TARGET_GROUP_ID in "${GROUP_IDS[@]}"
do
    LDAP_CN="$(grep ":${TARGET_GROUP_ID}:" /etc/group | cut -d':' -f1)"

    echo "dn: cn=$LDAP_CN,ou=Group,$SUFFIX" >> $LDIF
    echo "objectClass: posixGroup" >> $LDIF
    echo "cn: $LDAP_CN" >> $LDIF
    echo "gidNumber: $TARGET_GROUP_ID" >> $LDIF

    for MEMBER_UID in $(grep ":${TARGET_GROUP_ID}:" /etc/passwd | cut -d':' -f1,3)
    do
        UID_NUM=$(echo "$MEMBER_UID" | cut -d':' -f2)
        [ $UID_NUM -ge 1000 -a $UID_NUM -le 9999 ] && echo "memberUid: $(echo "$MEMBER_UID" | cut -d':' -f1)" >> $LDIF
    done
    echo >> $LDIF
done
)
EOF